# AWS Live Chat 💬

I created a live chat web app using AWS Amplify and Next JS. This project is part of a mentoring module at HSLU Lucerne with the goal to understand and apply cloud computing services.

## Check it out ✨

Create a account and write me a message.
Test the chat live on: https://chat.niklasbuchfink.com/

## Clone this project

First, it must be said that setting up an application with AWS services takes some time and may cost a little money.

### Setup AWS Amplify

1. Create an AWS account
2. Install AWS Amplify CLI:
```bash
npm install -g @aws-amplify/cli
```
3. Link the CLI to your AWS account:
```bash
amplify configure
```
Detailed information: [Amplify Setup](https://docs.amplify.aws/cli/start/install/)

### Deployment

1. Clone this Amplify project:
```bash
amplify init --app https://gitlab.com/Niklas.Buchfink/live-chat.git
```
2. Deploy to your cloud environment:
```bash
amplify push
```

Detailed information: [Clone Sample Amplify Project](https://docs.amplify.aws/cli/start/workflows/#clone-sample-amplify-project)

### Run Frontend locally

The frontend is build with [Next.js](https://nextjs.org/) and bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

#### Setup
Make sure to install the dependencies:
```bash
npm install
# or
yarn install
```

#### Development
Start the development server on http://localhost:3000:

```bash
npm run dev
# or
yarn dev
```

## Sources

The Amplify chat tutorial of [Jarrod Watts](https://blog.jarrodwatts.com/aws-amplify-live-chat) was a great starting point for me:

- [Tutorial video](https://www.youtube.com/watch?v=g2kMOr3d084)
- [Repository](https://github.com/jarrodwatts/amplify-live-chat)

I used this rich text editor with markdown capabilities for the chat input:

- [WYSIWIG Editor Repository](https://github.com/jpuri/react-draft-wysiwyg)
- [WYSIWIG Editor Documentation](https://jpuri.github.io/react-draft-wysiwyg/#/docs)

To get started and learn AWS Amplify:

- [Amplify Documentation](hhttps://docs.amplify.aws/cli/)

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs)
- [Interactive Next.js tutorial](https://nextjs.org/learn)
