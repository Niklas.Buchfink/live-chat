import '@aws-amplify/ui-react/styles.css';
import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import { API, Auth, graphqlOperation, withSSRContext } from 'aws-amplify'
import { EditorState, convertToRaw } from 'draft-js';
import React, { useEffect, useState } from 'react'

import Message from '../components/message'
import { Storage } from "@aws-amplify/storage";
import { createMessage } from '../graphql/mutations'
import dynamic from 'next/dynamic';
import { listMessages } from '../graphql/queries'
import { onCreateMessage } from '../graphql/subscriptions'
import styles from '../styles/Home.module.css'
import { withAuthenticator } from '@aws-amplify/ui-react'

const Editor = dynamic(
  () => import('react-draft-wysiwyg').then(mod => mod.Editor),
  { ssr: false }
)

async function getStoredImage(key) {
  try {
    // get the signed URL string
    const signedURL = await Storage.get(key, { expires: 604800 }); // week in seconds
    return(
      { data: { link: signedURL}}
    )
  } catch (error) {
    console.log("Error receiving file: ", error);
  }
}

async function uploadImageCallBack(file) {
  try {
    const { key } = await Storage.put(file.name, file, {
      contentType: "image/*",
    });    
    return(
      getStoredImage(key)
    );
  } catch (error) {
    console.log("Error uploading file: ", error);
  }
}

function Home({ messages }) {
  const [stateMessages, setStateMessages] = useState([...messages]);
  const [user, setUser] = useState(null);
  const [editorState, setEditorState] = useState(EditorState.createEmpty())
  const onEditorStateChange = (editorState) => { 
    setEditorState(editorState)
  }

  const fetchUser = async () => {
    try {
      const amplifyUser = await Auth.currentAuthenticatedUser();
      setUser(amplifyUser);
    } catch (err) {
      setUser(null);
    }
  };
  
  useEffect(() => {
    fetchUser();

    // Subscribe to creation of message
    API.graphql(
      graphqlOperation(onCreateMessage)
    ).subscribe({
      next: ({ provider, value }) => {
        setStateMessages((stateMessages) => [
          ...stateMessages,
          value.data.onCreateMessage,
        ]);
      },
      error: (error) => console.warn(error),
    });
  }, []);

  useEffect(() => {
    async function getMessages () {
      try {
        const messagesReq = await API.graphql({
          query: listMessages,
          authMode: "AMAZON_COGNITO_USER_POOLS",
        });
        setStateMessages([...messagesReq.data.listMessages.items]);
      } catch (error) {
        console.error(error);
      }
    }
    getMessages();
  }, [user]);

  const handleSubmit = async (event) => {
    // Prevent the page from reloading
    event.preventDefault();

    // => mod.draftToMarkdown(convertToRaw(editorState.getCurrentContent()))
    let obj = await import('draftjs-to-markdown')
    let draftToMarkdown = obj.default

    const input = {
      // id is auto populated by AWS Amplify
      message: draftToMarkdown(convertToRaw(editorState.getCurrentContent())), // the message content the user submitted (from state)
      owner: user.username, // this is the username of the current user
    };

    // clear the textbox
    setEditorState(EditorState.createEmpty());

    // Try make the mutation to graphql API
    try {
      await API.graphql({
        authMode: "AMAZON_COGNITO_USER_POOLS",
        query: createMessage,
        variables: {
          input: input,
        },
      });
    } catch (err) {
      console.error(err);
    }
  };

  if (user) {
    return (
      <div className={styles.background}>
        <div className={styles.container}>
          <h1 className="text-3xl"> AWS Amplify Live Chat</h1>
          <div className={styles.chatbox}>
            {stateMessages
              // sort messages oldest to newest client-side
              .sort((a, b) => b.createdAt.localeCompare(a.createdAt))
              .filter((value, index, self) =>
                index === self.findIndex((t) => (
                  t.id === value.id && t.message === value.message
                ))
              )
              .map((message) => (
                // map each message into the message component with message as props
                <Message
                  message={message}
                  user={user}
                  isMe={user.username === message.owner}
                  key={message.id}
                />
              ))}
          </div>
          <div className={styles.formContainer}>
            <form onSubmit={handleSubmit} className={styles.formBase}>
              <Editor
                wrapperClassName="demo-wrapper"
                editorClassName="demo-editor border rounded p-4 bg-white"
                toolbarClassName="toolbarClassName"
                toolbar={{
                  options: ['inline', 'link', 'list', 'blockType', 'emoji', 'image'],
                  inline: { 
                    options: ['bold', 'italic', 'strikethrough', 'monospace'],
                  },
                  link: { 
                    options: ['link'],
                  },
                  list: { 
                    options: ['unordered', 'ordered'],
                  },
                  blockType: {
                    inDropdown: false, 
                    options: ['Blockquote'],
                  },
                  image: { 
                    previewImage: true,
                    uploadCallback: uploadImageCallBack, alt: { present: true, mandatory: true } 
                  },
                }}
                mention={{
                  separator: ' ',
                  trigger: '@',
                  suggestions: [
                    { text: 'APPLE', value: 'apple', url: 'apple' },
                    { text: 'BANANA', value: 'banana', url: 'banana' },
                    { text: 'CHERRY', value: 'cherry', url: 'cherry' },
                    { text: 'DURIAN', value: 'durian', url: 'durian' },
                    { text: 'EGGFRUIT', value: 'eggfruit', url: 'eggfruit' },
                    { text: 'FIG', value: 'fig', url: 'fig' },
                    { text: 'GRAPEFRUIT', value: 'grapefruit', url: 'grapefruit' },
                    { text: 'HONEYDEW', value: 'honeydew', url: 'honeydew' },
                  ],
                }}
                hashtag={{}}
                placeholder="💬 Send a message to the world 🌎"
                editorState={editorState}
                onEditorStateChange={onEditorStateChange}
              />
              <button className="ml-2 px-4 py-2 bg-stone-800 rounded-md text-white">
                Send
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  } else {
    return <p>Loading...</p>;
  }
}

export default withAuthenticator(Home);

export async function getServerSideProps({ req }) {
  // wrap the request in a withSSRContext to use Amplify functionality serverside.
  const SSR = withSSRContext({ req });

  try {
    // currentAuthenticatedUser() will throw an error if the user is not signed in.
    const user = await SSR.Auth.currentAuthenticatedUser();

    // If we make it passed the above line, that means the user is signed in.
    const response = await SSR.API.graphql({
      query: listMessages,
      // use authMode: AMAZON_COGNITO_USER_POOLS to make a request on the current user's behalf
      authMode: "AMAZON_COGNITO_USER_POOLS",
    });

    // return all the messages from the dynamoDB
    return {
      props: {
        messages: response.data.listMessages.items,
      },
    };
  } catch (error) {
    // We will end up here if there is no user signed in.
    // We'll just return a list of empty messages.
    return {
      props: {
        messages: [],
      },
    };
  }
}
