import React from "react";
import styles from "../styles/Message.module.css";
import ReactMarkdown from "react-markdown";
import gfm from "remark-gfm";
import rehypeRaw from "rehype-raw";

export default function Message({ message, isMe }) {
  return (
    <div
      className={
        isMe ? styles.sentMessageContainer : styles.receivedMessageContainer
      }
    >
      <p className={styles.senderText}>{message.owner}</p>
      <div className={isMe ? styles.sentMessage : styles.receivedMessage}>
        <ReactMarkdown
          className="prose"
          remarkPlugins={[gfm]}
          rehypePlugins={[rehypeRaw]}
        >
          {message.message}
        </ReactMarkdown>
      </div>
    </div>
  )
}
