module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    fontFamily: {
      sans: ['sans-serif'],
      mono: ['monospace']
    },
    extend: {},
  },
  plugins: [],
  
  "tailwindCSS.includeLanguages": {
    "plaintext": "html,js,ts,jsx,tsx,css"
  }
}